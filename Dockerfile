#
# Casperjs + Phantomjs + Slimerjs on Centos Dockerfile
#
# https://80lder49@bitbucket.org/80lder49/skraper.git
#


FROM centos:latest

RUN yum update -y && \
    yum upgrade -y && \
    yum install -y \
        git \
        xorg-x11-server-Xvfb \
        xorg-x11-server-Xorg \
        xorg-x11-fonts* \
        dbus-x11 \
        xulrunner.x86_64 \
        nspr.x86_64 \
        nss.x86_64 \
        XvfbInit